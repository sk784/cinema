import 'package:flutter/material.dart';

class TicketCardItem extends StatelessWidget {
  final String name;
  final String date;
  final int ticketHall;
  final int ticketRow;
  final int ticketSeat;

  TicketCardItem(this.name,this.date,
      this.ticketHall,this.ticketRow,this.ticketSeat);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const Radius.circular(5.0)),
        border: Border.all(color: Color(0xFFF6F6F6)),
        color: Color(0xFFF6F6F6),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
              children: <Widget>[
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left:20.0, top: 16.0, bottom: 33.0),
                    child: Text(name,
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold
                      ),
                    )
                ),
              Container(
                  margin: const EdgeInsets.only(bottom: 2.0),
                   child:Row(
                    children: <Widget>[
                      Container(
                          margin: const EdgeInsets.only(left:20.0,right: 90.0),
                          child: Text('Дата и время',
                            style: TextStyle(
                                fontSize: 12.0,
                            ),
                          )
                      ),
                      Text('Зал',
                        style: TextStyle(
                          fontSize: 12.0,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 20.0,right: 20.0),
                        child: Text('Ряд',
                          style: TextStyle(
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Text('Место',
                        style: TextStyle(
                          fontSize: 12.0,
                        ),
                      ),
                    ]
                )
              ),
                Container(
                  margin: const EdgeInsets.only(bottom: 21.0),
                  child: Row(
                      children: <Widget>[
                        Container(
                            margin: const EdgeInsets.only(left:20.0,right: 60.0),
                            child: Text(date.toString(),
                               style: TextStyle(
                               fontSize: 12.0,
                                   fontWeight: FontWeight.bold
                               ),
                            )
                        ),
                        Container(
                            child: Text(ticketHall.toString(),
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold
                              ),
                            )
                        ),
                        Container(
                            margin: const EdgeInsets.only(left: 36.0,right: 36.0),
                            child: Text(ticketRow.toString(),
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold
                              ),
                            )
                        ),
                        Container(
                            child: Text(ticketSeat.toString(),
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold
                              ),
                            )
                        ),
                      ]
                  ),
                ),
              ]
      ),
    );
  }
}