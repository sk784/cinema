import 'package:flutter/material.dart';

class NewFilmsItem extends StatelessWidget {
  final String url;
  final String name;
  final String genre;
  final String country;
  final int minPrice;
  final int maxPrice;
  final String restriction;

  NewFilmsItem(this.url,this.name,this.genre,this.country,this.minPrice,
      this.maxPrice,this.restriction);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
      BoxDecoration(
          color: Color(0xFFF6F6F6),
          borderRadius: BorderRadius.all(const Radius.circular(5.0)),
          border: Border.all(color: Color(0xFFF6F6F6))),
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: 270.0,
            child: Row(
               children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 12.0,left: 10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/placeholder.jpg",
                        image: url,
                        fit: BoxFit.cover,
                        width: 120.0,
                        height: 150.0,
                        fadeInDuration: Duration(milliseconds: 50),
                      )
                  )
                ),
                Flexible(
                  child: Container(
                    height: 150.0,
                    margin: const EdgeInsets.only(top: 12.0),
                    child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: <Widget>[
                             Text(name,
                               overflow: TextOverflow.visible,
                               style: TextStyle(
                                 fontSize: 18.0,
                                 fontWeight: FontWeight.bold,
                               ),
                             ),
                             Container(
                                 margin: const EdgeInsets.only(top: 4.0),
                                 child: Text(genre +" / "+country,
                                   style: TextStyle(
                                     fontSize: 12.0,
                                   ),
                                   overflow: TextOverflow.visible,
                                 )
                             ),
                             Container(
                                 margin: const EdgeInsets.only(top: 20.0, bottom: 16.0),
                                 child: Text('от $minPrice \u20BD до $maxPrice \u20BD',
                                   style: TextStyle(
                                       fontSize: 18.0,
                                       fontWeight: FontWeight.bold
                                   ),
                                 )
                             ),
//                   Row(
//                     mainAxisSize: MainAxisSize.min,
//                     children: [
//                       Icon(
//                           stars > 0 ? Icons.star : Icons.star_border,
//                           size: 16.0),
//                       Icon(stars > 1 ? Icons.star : Icons.star_border,
//                           size: 16.0
//                       ),
//                       Icon(stars > 2 ? Icons.star : Icons.star_border,
//                           size: 16.0
//                       ),
//                       Icon(stars > 3 ? Icons.star : Icons.star_border,
//                           size: 16.0
//                       ),
//                       Icon(stars > 4 ? Icons.star : Icons.star_border,
//                           size: 16.0
//                       ),
//                     ],
//                   )
                           ]
                       ),
                  ),
                ),
                ]
        ),
          ),
          Container(
              margin: const EdgeInsets.only(bottom:120.0, right: 16.0),
              child:Text(restriction+"+",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              )
          ),
        ]
      ),
    );
  }
}