import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class HTMLPage extends StatelessWidget {
final String url;

HTMLPage(this.url);

@override
Widget build(BuildContext context) {
 return Scaffold(
   appBar: AppBar(
     backgroundColor: Theme.of(context).scaffoldBackgroundColor,
     brightness: Theme.of(context).brightness,
     iconTheme: Theme.of(context)
    .iconTheme
    .copyWith(color: Theme.of(context).accentColor),
   ),
   body: Html(
     data: url,
     padding: EdgeInsets.all(16.0),
   )
 );
}
}