import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class HomePageVideoWidget extends StatefulWidget {

  final String title;
  final String urlVideo;
  final String urlPhoto;
  final String restriction;

  const HomePageVideoWidget({Key key, @required this.title, @required this.urlPhoto,
    @required this.urlVideo,@required this.restriction})
      : super(key: key);

  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<HomePageVideoWidget> {
  VideoPlayerController _videoPlayerController ;
  Future<void> _initializeVideoPlayerFuture;
  bool _isVideo = false;
  bool _isYouTube;
  YoutubePlayerController _youtubePlayerController;

  @override
  void initState() {
    widget.urlVideo.contains("https://www.youtube.com/watch?")
        ?
    _isYouTube = true
        :
    _isYouTube = false;

    _videoPlayerController = VideoPlayerController.network(widget.urlVideo);
    _initializeVideoPlayerFuture = _videoPlayerController.initialize();
    _videoPlayerController.setLooping(true);

    _isYouTube == true
        ?
    _youtubePlayerController = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(widget.urlVideo),
        flags: YoutubePlayerFlags(
            autoPlay: true
        )
    )
        :
    null;
    super.initState();
  }

  @override
  void didUpdateWidget(HomePageVideoWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.urlVideo != oldWidget.urlVideo) {
      widget.urlVideo.contains("https://www.youtube.com/watch?")
          ?
      _isYouTube = true
          :
      _isYouTube = false;

      _videoPlayerController = VideoPlayerController.network(widget.urlVideo);
      _initializeVideoPlayerFuture = _videoPlayerController.initialize();
      _videoPlayerController.setLooping(true);

      _isYouTube == true
          ?
      _youtubePlayerController = YoutubePlayerController(
          initialVideoId: YoutubePlayer.convertUrlToId(widget.urlVideo),
          flags: YoutubePlayerFlags(
              autoPlay: true
          )
      )
          :
      null;
    }
  }

    @override
    Widget build(BuildContext context) {
      return Container(margin: const EdgeInsets.symmetric(horizontal: 16.0),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Container(
                height: 212.0,
                child: Stack(children: <Widget>[
                  Container(
                   child: _isVideo
                       ?
                    _isYouTube
                        ?
                    Container(
                        width: MediaQuery.of(context).size.width,
                        height: 212,
                        child: YoutubePlayer(
                          controller: _youtubePlayerController,
                        )
                    )
                        :
                    FutureBuilder(
                     future: _initializeVideoPlayerFuture,
                     builder: (context, snapshot) {
                     if (snapshot.connectionState ==
                        ConnectionState.done) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        height: 212,
                        child: VideoPlayer(_videoPlayerController),
                      );
                    } else {
                      return Center(
                          child: CircularProgressIndicator());
                    }
                  },
                 )
                    :
                   Container(
                     width: MediaQuery.of(context).size.width,
                     height: 212,
                   child: FadeInImage.assetNetwork(
                     placeholder: "assets/placeholder.jpg",
                     image: widget.urlPhoto,
                     fit: BoxFit.fill,
                     fadeInDuration: Duration(milliseconds: 50),
                   ),
                 )
                 ),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children:  <Widget>[
                     Container(
                       width: 250,
                       child: Row(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: <Widget>[
                         Container(
                          margin: const EdgeInsets.only(left: 16.0,
                           right: 16.0, top: 152.0),
                          height: 44.0,
                           width: 44.0,
                           child: FittedBox(
                             child: FloatingActionButton(
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black,
                              onPressed: () {
                              setState(() {
                              _isVideo = !_isVideo;
                              }
                              );
                              if(_isYouTube){
                                _youtubePlayerController.value.isPlaying
                                    ?
                                _youtubePlayerController.pause()
                                    :
                                _youtubePlayerController.play();
                              } else {
                              _videoPlayerController.value.isPlaying
                               ?
                              _videoPlayerController.pause()
                               :
                              _videoPlayerController.play();
                              }
                              },
                              child: _isYouTube
                                  ?
                              Icon(_isVideo
                              ?
                              Icons.pause
                                  :
                              LineIcons.play,
                              size: 30.0,)
                                :
                              Icon(_videoPlayerController.value.isPlaying
                                ?
                              Icons.pause
                                  :
                              LineIcons.play,
                              size: 30.0,)
                           ),
                         )
                        ),
                        Flexible(
                          child: Container(
                              margin: const EdgeInsets.only(top: 153.0),
                              child: Text(widget.title,
                             style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                               color: Colors.white
                           ),
                         )
                          ),
                         ),
                          ]
                         ),
                     ),
                     Container(
                         margin: const EdgeInsets.only(top: 153.0,right: 16.0),
                         child: Text(widget.restriction+"+",
                           style: TextStyle(
                               fontSize: 24.0,
                               color: Colors.white
                           ),
                         )
                     ),
                   ],
                 ),
          ]
      )
      ),
      ),
      );
    }

  @override
  void dispose() {
    _isYouTube
        ?
    _youtubePlayerController.dispose()
        :
    _videoPlayerController.dispose();
    super.dispose();
  }
}


