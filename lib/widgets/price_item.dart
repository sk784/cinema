import 'package:flutter/material.dart';

class PriceItem extends StatelessWidget {

  final int time;
  final double price;
  final bool isActual;

  PriceItem(this.time, this.price,this.isActual);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(right: 12.0),
        width: 76.0,
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: isActual
                ?
            Color(0xFF8A8A8A)
                :
            Color(0xFFBBBBBB))
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(left: 10.0, top: 7.0, bottom: 2.0,
                  right: 10.0),
              child: Text(time.toString().length==4
                  ?
              time.toString().replaceRange(2, 2,":")
                  :
              time.toString().replaceRange(1, 1,":"),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0,
                ),
              ),
            ),
            Text('${price.toInt()}\u20BD',
            ),
          ],
        )
    );
   }
}