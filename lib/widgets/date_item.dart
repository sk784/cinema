import 'package:cinema/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateItem extends StatefulWidget {

  final int day;
  final int index;
  final bool isSelected;
  final VoidCallback onSelect;

  const DateItem({
    Key key,
    this.index,
    this.isSelected,
    this.onSelect,
    this.day,
  }) : super(key: key);

  @override
  _DateItemState createState() => _DateItemState();
}

class _DateItemState extends State<DateItem> {


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onSelect,
      child: Container(
        width: 44.0,
        margin: const EdgeInsets.only(right: 12.0),
        padding: const EdgeInsets.only(top: 7.0),
        child: Column(
            children: <Widget>[
              Text(widget.day.toString().substring(4,6),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0,
                  color:  widget.isSelected
                      ?
                  Colors.white
                      :
                  Theme.of(context).brightness ==
                      Brightness.dark
                      ?
                  Colors.white
                      :
                  Colors.black,
                ),
              ),
              Text(formatWeekDay(DateFormat("yyyy/MM/dd").parse(widget.day.toString().
                    replaceRange(0, 0,"20").replaceRange(4, 4,"/").replaceRange(7, 7,"/")).weekday),
                style: TextStyle(
                  fontSize: 10.0,
                  color:  widget.isSelected
                      ?
                  Colors.white
                      :
                  Theme.of(context).brightness ==
                      Brightness.dark
                      ?
                  Colors.white
                      :
                  Colors.black,
                ),
              )
          ]
        ),
        decoration: widget.isSelected
            ?
        BoxDecoration(color: Colors.blue, border: Border.all(color: Colors.blue),
        borderRadius: BorderRadius.circular(5.0))
            :
        BoxDecoration(),
        )
    );
  }
}