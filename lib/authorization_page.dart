import 'package:cinema/screens/auth/signin.dart';
import 'package:cinema/tabs_screen.dart';
import 'package:flutter/material.dart';
import 'model/api_client.dart';
import 'package:http/http.dart' as http;


class AuthorizationPage extends StatefulWidget {

  @override
  State<StatefulWidget>createState() =>AuthorizationPageState();
}

enum AuthStatus { notSignedIn, signedIn, loading }

class AuthorizationPageState extends State<AuthorizationPage> {

  AuthStatus _authStatus = AuthStatus.notSignedIn;
  var service = MovieApi.instance;

  @override
  void initState() {
    super.initState();
    _authStatus = AuthStatus.loading;

    void _processResponse(http.Response response) {
      if(response.statusCode == 200){
        print(response.body);
        setState(() {
          _authStatus = AuthStatus.signedIn;
        });
      }
      else if (response.statusCode == 401){
        print(response.statusCode);
        setState(() {
          _authStatus = AuthStatus.notSignedIn;
        });
      }
      else {
        print(response.statusCode);
      }
    }

    service.userLoginByToken().then((_processResponse))
        .catchError((error){
      print('error : $error');
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (_authStatus) {
      case AuthStatus.loading:
        return Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
    case AuthStatus.notSignedIn:
    return SignIn();
    case AuthStatus.signedIn:
    return TabsScreen();
    }
    return SignIn();
  }
}