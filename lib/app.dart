import 'package:cinema/authorization_page.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MyApp extends StatelessWidget {

  final String title;

  const MyApp({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final orange = Color.fromARGB(255, 240, 89, 41);
    final blue = Color.fromARGB(255, 33, 153, 227);
    return DynamicTheme(
        defaultBrightness: Brightness.light,
        data: (brightness) =>
            ThemeData(
              primaryColor: blue,
              scaffoldBackgroundColor: brightness == Brightness.light
                  ?
              Colors.white
                  :
              Colors.grey[850],
              accentColor: orange,
              toggleableActiveColor: orange,
              dividerColor:
              brightness == Brightness.light
                  ?
              Colors.white
                  :
              Colors.white54,
              brightness: brightness,
              fontFamily: 'PTSans',
              bottomAppBarTheme: Theme
                  .of(context)
                  .bottomAppBarTheme
                  .copyWith(
                elevation: 0,
              ),
              iconTheme: Theme
                  .of(context)
                  .iconTheme
                  .copyWith(color: orange),
            ),
        themedWidgetBuilder: (context, theme) {
          return MaterialApp(
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('en', 'US'),
              const Locale('ru', 'RU'),
            ],
            debugShowCheckedModeBanner: false,
            title: title,
            theme: theme,
            home: AuthorizationPage(),
          );
        });
  }
}




