import 'dart:async';
import 'dart:convert';

import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/entity/account/notifications.dart';
import 'package:cinema/model/entity/account/update_profile.dart';
import 'package:cinema/utils/phone_form_format.dart';
import 'package:cinema/widgets/settings_toggle.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'change_password.dart';
import 'code.dart';

class ChangeProfile extends StatefulWidget {

  const ChangeProfile({Key key, this.name,this.phone, this.notifications})
      : super(key: key);
  final String name;
  final String phone;
  final bool notifications;

  @override
  _ChangeProfileState createState() => _ChangeProfileState();
  }

class _ChangeProfileState extends State<ChangeProfile> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _wrongData = false;
  bool _isLoading = false;
  bool _notifications = true;
  bool _autoValidateName = true;
  bool _autoValidatePhone = true;
  final RusNumberTextInputFormatter _phoneNumberFormatter =
  RusNumberTextInputFormatter();
  TextEditingController _tecName = new TextEditingController();
  TextEditingController _tecPhone = new TextEditingController();
  var service = MovieApi.instance;

  @override
  void initState() {
    super.initState();
    _tecName.text = widget.name;
    _tecPhone.text = widget.phone.substring(2);
    _notifications = widget.notifications;
    _tecName.addListener(() => setState(() {
      _autoValidateName = _tecName.text.length > 0
          ?
      true
          :
      false;
    }));
    _tecPhone.addListener(() => setState(() {
      _autoValidatePhone = _tecPhone.text.length > 12
          ?
      true
          :
      false;
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),
        ),
        body:  _isLoading
            ?
        Center(
          child: CircularProgressIndicator(),
        )
            :
        SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 24.0,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, bottom: 24.0),
                    child: Text(
                      'Изменение профиля',
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                        //   color: Colors.white,
                      ),
                    )
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, bottom: 6.0),
                    child: Text(
                      'Имя',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )
                ),
                buildNameTextField(),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,
                        bottom: 6.0, top: 10.0),
                    child: Text(
                      'Телефон',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )
                ),
                buildPhoneTextField(),
                changePasswordButton(),
                SettingsToggle(
                  title: 'Уведомления из приложения',
                  onChanged: (bool isOn) {
                    setState(() {
                      _notifications = isOn;
                      isOn =!isOn;
                    });
                  },
                  value: _notifications
                ),
                SizedBox(
                  height: 240.0,
                ),
                confirmButton(),
              ],
            )
        )
    );
  }

  Widget buildNameTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
          child: TextFormField(
            controller: _tecName,
            keyboardType: TextInputType.text,
          ),
        ),
      );

  Widget buildPhoneTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 24.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
          child: TextFormField(
            controller: _tecPhone,
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly,
              _phoneNumberFormatter
            ],
            keyboardType: TextInputType.phone,
            decoration:  InputDecoration(
                prefixText: '+7',
                prefixStyle: TextStyle(color: Theme.of(context).brightness ==
                    Brightness.dark
                    ?
                Colors.white
                    :
                Colors.black,
                    fontSize: 16.0)
            ),
          ),
        ),
      );

  Widget changePasswordButton() =>
     Padding(padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: SizedBox(
            height: 46.0,
            child: OutlineButton(
                onPressed: _changePassword,
                borderSide:
                BorderSide(color: Colors.blue, width: 1.0),
                child: Text(
                  'Изменить пароль',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.blue,
                  ),
                )
            ),
          )
      );


  void _changePassword() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>
          ChangePassword()),
    );
  }

  Widget confirmButton() =>
      Padding(padding: EdgeInsets.only(left: 16.0,right: 16.0,bottom: 24.0),
          child: SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed:_autoValidateName ==false||_autoValidatePhone==false
                    ?
                null
                    :
                    () {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  'Сохранить изменения',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
            ),
          )
      );

  void _confirmPressed() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    if(_tecPhone.text!=widget.phone.substring(2)){
      UpdateProfile updateProfile = UpdateProfile(_tecName.text, null,
         null, null, null, "+7"+_tecPhone.text.replaceAll("(", "").
          replaceAll(")", "").replaceAll("-", ""),
          _notifications==true
              ?
          Notifications(1,1)
              :
          Notifications(0,0));
      String jsonUpdateProfile = jsonEncode(updateProfile);
      print(jsonUpdateProfile);

      service.userUpdate(jsonUpdateProfile).then((response){
        if (response.statusCode == 202){
          setState(() {
            _isLoading = false;
            _wrongData= false;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Успешно, код на новый телефон отправлен!"),
            duration: Duration(seconds: 1),
            backgroundColor:Colors.green,
          ));
          print(response.statusCode);
          Timer(Duration(seconds: 1), () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    Code(phone: "+7"+_tecPhone.text)));
          });
        }
        else if (response.statusCode == 401){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Ошибка авторизации"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else if (response.statusCode == 409){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Такой номер уже зарегистрирован!"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else if (response.statusCode == 429){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("С момента предыдущей отправки кода на этот "
                "телефон не прошло 3 минут"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else if (response.statusCode == 450){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("На этот номер отправка невозможна"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else {
          print(response.statusCode);
          setState(() {
            _isLoading = false;
          });
        }
      }).catchError((error){
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неизвестная ошибка"),
          backgroundColor:Colors.red,
        ));
        setState(() {
          _isLoading = false;
          _wrongData= true;
        });
        print('error : $error');
      });
    } else if ((_tecName.text!=widget.name)||(_notifications!=widget.notifications)){
      UpdateProfile updateProfile = UpdateProfile(_tecName.text, null,
          null, null, null, null,
          _notifications==true
              ?
          Notifications(1,1)
              :
          Notifications(0,0));
      String jsonUpdateProfile = jsonEncode(updateProfile);
      print(jsonUpdateProfile);

      service.userUpdate(jsonUpdateProfile).then((response){
        if (response.statusCode == 200){
          setState(() {
            _isLoading = false;
            _wrongData= false;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Успешно, данные изменены!"),
            backgroundColor:Colors.green,
          ));
          print(response.statusCode);
        }
        else if (response.statusCode == 401){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Ошибка авторизации"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else {
          print(response.statusCode);
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
        }
      }).catchError((error){
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неизвестная ошибка"),
          backgroundColor:Colors.red,
        ));
        setState(() {
          _isLoading = false;
          _wrongData= true;
        });
        print('error : $error');
      });
    }
    else {
      setState(() {
        _isLoading = false;
      });
      print("Нет изменений!");
    }
  }

  @override
  void dispose() {
    _tecName.dispose();
    _tecPhone.dispose();
    super.dispose();
  }
}