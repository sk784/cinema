import 'dart:convert';
import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/entity/account/update_profile.dart';
import 'package:flutter/material.dart';

class ChangePassword extends StatefulWidget {

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  TextEditingController _tecOldPass = TextEditingController();
  TextEditingController _tecNewPass = TextEditingController();
  TextEditingController _tecRepeatNewPass = TextEditingController();
  bool _autoValidateOldPass = false;
  bool _autoValidateNewPass = false;
  bool _autoValidateRepeatNewPass = false;
  bool _wrongData = false;
  FocusNode _newPassFocusNode;
  FocusNode _repeatNewPassFocusNode;
  var service = MovieApi.instance;

  @override
  void initState() {
    super.initState();
    _newPassFocusNode = FocusNode();
    _repeatNewPassFocusNode = FocusNode();
    _tecOldPass.addListener(() => setState(() {
      _autoValidateOldPass = _tecOldPass.text.length > 0
          ?
      true
          :
      false;
    }));

    _tecNewPass.addListener(() => setState(() {
      _autoValidateNewPass = _tecNewPass.text.length > 0
          ?
      true
          :
      false;
    }));

    _tecRepeatNewPass.addListener(() => setState(() {
      _autoValidateRepeatNewPass = _tecRepeatNewPass.text.length > 0
          ?
      true
          :
      false;
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),
        ),
        body:  _isLoading
            ?
        Center(
          child: CircularProgressIndicator(),
        )
            :
        SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 24.0,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, bottom: 24.0),
                    child: Text(
                      'Изменение пароля',
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                        //   color: Colors.white,
                      ),
                    )),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, bottom: 10.0),
                    child: Text(
                      'Старый пароль',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )
                ),
                buildOldPassTextField(context),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,
                        bottom: 10.0),
                    child: Text(
                      'Новый пароль',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                        fontSize: 12.0,
                      ),
                    )
                ),
                buildNewPassTextField(context),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,
                        bottom: 10.0),
                    child: Text(
                      'Повторите новый пароль',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                         fontSize: 12.0,
                      ),
                    )
                ),
                buildRepeatNewPassTextField(context),
                SizedBox(
                  height: 270.0,
                ),
                confirmButton(),
              ],
            )
        )
    );
  }

  Widget buildOldPassTextField(BuildContext context) =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1):
            Colors.red)
        ),
        child: TextFormField(
          obscureText: true,
          controller: _tecOldPass,
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(_newPassFocusNode);
          },
        ),
      );

  Widget buildNewPassTextField(BuildContext context) =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          obscureText: true,
          controller: _tecNewPass,
          focusNode: _newPassFocusNode,
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(_repeatNewPassFocusNode);
          },
        ),
      );

  Widget buildRepeatNewPassTextField(BuildContext context) =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          obscureText: false,
          controller: _tecRepeatNewPass,
          focusNode: _repeatNewPassFocusNode,
        ),
      );


  Widget confirmButton() =>
       Padding(padding: EdgeInsets.only(left:16.0,right: 16.0, bottom: 24.0),
          child: SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed:_autoValidateOldPass ==false||_autoValidateNewPass==false||
                _autoValidateRepeatNewPass ==false
                    ?
                null
                    :
                    () {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  'Сохранить пароль',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
            ),
          )
      );

  void _confirmPressed() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    if(_tecNewPass.text==""||_tecRepeatNewPass.text==""){
      setState(() {
        _isLoading = false;
        _wrongData= true;
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Введите новый пароль!"),
        backgroundColor:Colors.red,
      ));
    }
    else if(_tecNewPass.text!=_tecRepeatNewPass.text) {
      setState(() {
        _isLoading = false;
        _wrongData= true;
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Пароли не совпадают!"),
        backgroundColor: Colors.red,
      ));
    } else if(_tecNewPass.text==_tecOldPass.text){
      setState(() {
        _isLoading = false;
        _wrongData= true;
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Пароль не изменился!"),
        backgroundColor:Colors.red,
      ));
     } else {
      UpdateProfile updateProfile = UpdateProfile(null, _tecOldPass.text,
          _tecNewPass.text, null, null, null, null);
      String jsonUpdateProfile = jsonEncode(updateProfile);
      print(jsonUpdateProfile);

      service.userUpdate(jsonUpdateProfile).then((response){
        if(response.statusCode == 200){
          setState(() {
            _isLoading = false;
            _wrongData= false;
          });
          print(response.statusCode);
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Успешно, пароль изменен!"),
            backgroundColor:Colors.green,
          ));
        }
        else if (response.statusCode == 401){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Ошибка авторизации"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else if (response.statusCode == 403){
          setState(() {
            _isLoading = false;
            _wrongData= true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Неверный старый пароль"),
            backgroundColor:Colors.red,
          ));
          print(response.statusCode);
        }
        else {
          print(response.statusCode);
          setState(() {
            _isLoading = false;
          });
        }
      }).catchError((error){
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неизвестная ошибка"),
          backgroundColor:Colors.red,
        ));
        setState(() {
          _isLoading = false;
          _wrongData= true;
        });
        print('error : $error');
      });
    }
  }

  @override
  void dispose() {
    _tecOldPass.dispose();
    _tecNewPass.dispose();
    _tecRepeatNewPass.dispose();
    _newPassFocusNode.dispose();
    _repeatNewPassFocusNode.dispose();
    super.dispose();
  }
}