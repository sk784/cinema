import 'dart:convert';

import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/account/user.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'change_profile.dart';
import 'package:http/http.dart' as http;

class MainProfile extends StatefulWidget {

  MainProfile({Key key}) : super(key: key);

  @override
  _MainProfileState createState() => _MainProfileState();
}

class _MainProfileState extends State<MainProfile>{
  String _userName = "";
  String _userPhone = "";
  bool _userNotifications;
  String _userUrl;
  var service = MovieApi.instance;
  User _user;

  @override
  void initState() {
    super.initState();
    _getUserData();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),

        ),
        body: Container(
          padding: EdgeInsets.only(left: 16.0,top: 24.0),
          child:
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      width: 90.0,
                      height: 90.0,
                      color: Color(0xFFF6F6F6),
                      child: Stack(
                          children: <Widget>[
                            _userUrl!= null
                            ?
                          Container(
                            width: 90.0,
                             height: 90.0,
                             decoration: BoxDecoration(
                                 image: DecorationImage(
                                 fit: BoxFit.fill,
                                  image: NetworkImage(_userUrl),
                                    ),
                                 ),
                                )
                            :
                            Center(
                              child: Icon(
                                LineIcons.user,size: 50,color: Colors.black,
                              ),
                            ),
                           Container(
                             padding: const EdgeInsets.all(5.0),
                             alignment: Alignment.bottomRight,
                              child: GestureDetector(
                               child: CircleAvatar(
                                  backgroundColor: Color(0xFFD8D8D8),
                                  radius: 10,
                                  child: Icon(Icons.add,
                                   size: 16.0,
                                   color: Colors.black,
                                  ),
                                 ),
                                 onTap: (){
                                print('1');
                               },
                             )
                            ),
                          ]
                        )
                      ),
                  Container(
                      margin: const EdgeInsets.only(left: 20.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(bottom: 4.0),
                              child: Text(_userName,
                                style: TextStyle(
                                fontSize: 24.0,
                                fontWeight: FontWeight.bold,
                              ),
                             ),
                             ),
                            Text(_userPhone,
                             style: TextStyle(
                             fontSize: 16.0,
                              ),
                             ),
                             Container(
                               margin: const EdgeInsets.only(top: 9.0),
                                child: InkWell(
                                  onTap: () {
                                   Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) =>
                                        ChangeProfile(name: _userName,
                                        phone: _userPhone,
                                           notifications: _userNotifications,
                                        )
                                    )
                                   );
                                 },
                                  child: Text(
                                  'Изменить профиль',
                                  style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue
                                ),
                              )
                          )
                      ),
                  ]
              )
             )
            ]
        ),
        )
        );
  }

  void _getUserData() {
    void _processResponse(http.Response response) {
      if(response.statusCode == 200){
        print(response.body);
          _user = User.fromJson(json.decode(response.body));
          _userName = _user.name;
          _userPhone = _user.phone.replaceRange(2, 2,"(").replaceRange(6, 6,")")
        .replaceRange(10, 10,"-").replaceRange(13, 13,"-");
        _user.aggregator == 1
        ?
        _userNotifications = true
        :
        _userNotifications = false;
      }
      else {
        print(response.statusCode);
      }
    }

    service.userLoginByToken()
        .then((_processResponse))
        .catchError((error){
      print('error : $error');
    });
  }
}
