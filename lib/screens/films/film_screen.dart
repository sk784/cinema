import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/films/film_schedule.dart';
import 'package:cinema/utils/utils.dart';
import 'package:cinema/widgets/date_item.dart';
import 'package:cinema/widgets/price_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:line_icons/line_icons.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';


class FilmScreen extends StatefulWidget {

  final String id;
  final String title;
  final String restriction;
  final int duration;
  final String genre;
  final String synopsis;
  final String urlPhoto;
  final String urlVideo;

  const FilmScreen({Key key, this.id, this.title,this.restriction,
  this.duration,this.genre,this.synopsis,this.urlPhoto,
  this.urlVideo}) : super(key: key);

  @override
  _FilmScreenState createState() => _FilmScreenState();
}

class _FilmScreenState extends State<FilmScreen> {

 VideoPlayerController _controller;
 Future<void> _initializeVideoPlayerFuture;
 bool _isVideo = false;
 int _currentSelectedIndex = 0;
 YoutubePlayerController _youtubePlayerController;
 bool _isYouTube;
 var service = MovieApi.instance;
 Future<List<FilmSchedule>> getMoviesSchedule;


  @override
  void initState() {
    setState(() {
      getMoviesSchedule = service.getMoviesSchedule(widget.id);
    });
    super.initState();
    widget.urlVideo.contains("https://www.youtube.com/watch?")
        ?
    _isYouTube = true
        :
    _isYouTube = false;
    _controller = VideoPlayerController.network(widget.urlVideo);
    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);

    _isYouTube == true
        ?
    _youtubePlayerController = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(widget.urlVideo),
        flags: YoutubePlayerFlags(
            autoPlay: true
        )
    )
        :
    null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              brightness: Brightness.light,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color:  Theme.of(context).accentColor),
                onPressed: () {
                  Navigator.pop(context);
                  },
                ),
                backgroundColor: Colors.transparent,
                expandedHeight: 312,
                flexibleSpace: FlexibleSpaceBar(
                  background:  buildAppBar(),
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  Container(
                      margin: const EdgeInsets.only(left: 16.0,right: 16.0, top: 23.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                    margin: const EdgeInsets.only(left: 85.0,bottom: 4.0),
                                    child: Text(formatRuntime(widget.duration),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                ),
                                Container(
                                    margin: const EdgeInsets.only(right: 81.0,bottom: 4.0),
                                    child: Text(widget.genre,
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                ),
                              ]
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                    margin: const EdgeInsets.only(left: 80.0),
                                    child: Text('Длительность',style: TextStyle(
                                        fontSize: 10.0,
                                    ),
                                    )
                                ),
                                Container(
                                    margin: const EdgeInsets.only(right: 100.0),
                                    child: Text('Жанр',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                    ),
                                    )
                                ),
                              ]
                          ),
                          Container(
                              margin: const EdgeInsets.only(top:16.0),
                              child: Text(widget.synopsis,
                                style: TextStyle(
                                  fontSize: 12.0,
                                ),
                              )
                          ),
                          Container(
                              margin: const EdgeInsets.only(top:30.0,bottom: 14.0),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Расписание сеансов',
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold
                                ),
                              )
                          ),
                      FutureBuilder(
                       future: getMoviesSchedule,
                       builder: (BuildContext context, AsyncSnapshot snapshot) {
                         if (snapshot.data != null && snapshot.data.toString()!="[]" ) {
                           final List<FilmSchedule> filmSchedule = snapshot.data;
                           return
                             Column(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                   Container(
                                     alignment: Alignment.topLeft,
                                     margin: const EdgeInsets.only(bottom: 32.0),
                                     height: 44.0,
                                     child: ListView.builder(
                                       scrollDirection: Axis.horizontal,
                                       itemCount: filmSchedule.length,
                                       shrinkWrap: true,
                                       physics: const ClampingScrollPhysics(),
                                       itemBuilder: (BuildContext context, int index) {
                                         return DateItem(
                                             day: filmSchedule[index].day,
                                             index: index,
                                             isSelected: _currentSelectedIndex == index,
                                             onSelect: () {
                                               setState(() {
                                                 _currentSelectedIndex = index;
                                               });
                                             }
                                         );
                                       },
                                     ),
                                   ),
                                   Container(
                                     height: 52.0,
                                     margin: const EdgeInsets.only(bottom: 65.0),
                                     alignment: Alignment.topLeft,
                                     child: ListView.builder(
                                       scrollDirection: Axis.horizontal,
                                       itemCount:filmSchedule[_currentSelectedIndex].prices.length,
                                       shrinkWrap: true,
                                       physics: const ClampingScrollPhysics(),
                                       itemBuilder: (BuildContext context, int index) {
                                         return Container(
                                             child: PriceItem(
                                                 filmSchedule[_currentSelectedIndex].prices.elementAt(index).time,
                                                 filmSchedule[_currentSelectedIndex].prices.elementAt(index).price,
                                                 (DateFormat("yyMMdd").format(DateTime.now()) ==
                                                     ((filmSchedule[_currentSelectedIndex].day).toString()))
                                                 && (filmSchedule[_currentSelectedIndex].prices.elementAt(index).time
                                                     < int.parse(DateFormat("HH:mm").format(DateTime.now()).replaceAll(":", "")))
                                                 ?
                                                 false
                                                     :
                                                 true
                                             )
                                         );
                                       },
                                     ),
                                   ),
                                 ]
                             );
                         } else if (snapshot.data.toString()=="[]"){
                           return Container(
                             child: Center(
                               child: Text(
                               'Для этого фильма еще нет расписания сеансов!',
                               style: TextStyle(
                               fontSize: 16.0,
                               color:  Color(0xFF8A8A8A),
                               fontWeight: FontWeight.bold
                           ),
                             ),
                           )
                           );
                         } else if (snapshot.hasError) {
                           return Padding(
                             padding: const EdgeInsets.only(top: 16.0,left: 16.0),
                             child: Text('Ошибка при загрузке данных: ${snapshot.error}'),
                           );
                         } else {
                           return Container(
                               child: Center(
                                 child: CircularProgressIndicator(),
                               )
                           );
                         }
                       }
                      )
                        ]
                    )
                  )
                ]),
              )
            ],
          ),
        ),
    );
  }


  Widget buildAppBar() =>
      Container(
          width: MediaQuery.of(context).size.width,
          child: Stack(children: <Widget>[
            Container(
              child: _isVideo
                  ?
              _isYouTube
                  ?
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: 332,
                  child: YoutubePlayer(
                    controller: _youtubePlayerController,
                  )
              )
                  :
              FutureBuilder(
                future: _initializeVideoPlayerFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: 332,
                      child: VideoPlayer(_controller),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                  },
              )
                  :
              Container(
                   width: MediaQuery.of(context).size.width,
                   height: 332,
                   child: FadeInImage.assetNetwork(
                     placeholder: "assets/placeholder.jpg",
                     image: widget.urlPhoto,
                     fit: BoxFit.fill,
                     fadeInDuration: Duration(milliseconds: 50),
             ),
              ),
            ),
           Column(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Container(
                 alignment: Alignment.topRight,
                 margin: const EdgeInsets.only(top: 45.0,right: 16.0),
                 child: Text(widget.restriction+'+',
                  style: TextStyle(
                   fontSize: 24.0,
                      color: Colors.white
                      ),
                    )
               ),
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: <Widget>[
                Flexible(
                 child: Container(
                   alignment: Alignment.topLeft,
                   margin: const EdgeInsets.only(left: 16.0,bottom: 35.0),
                   child: Text(widget.title,
                     style: TextStyle(
                     fontSize: 24.0,
                     fontWeight: FontWeight.bold,
                         color: Colors.white
                       ),
                    )
                   ),
                  ),
                   Container(
                     margin: const EdgeInsets.only(right: 21.0,bottom: 35.0),
                     height: 44.0,
                     width: 44.0,
                      child: FittedBox(
                        child: FloatingActionButton(
                         backgroundColor: Colors.white,
                           foregroundColor: Colors.black,
                         onPressed:() {
                         setState(() {
                         _isVideo=!_isVideo;
                          }
                          );
                         if(_isYouTube){
                         _youtubePlayerController.value.isPlaying
                             ?
                         _youtubePlayerController.pause()
                             :
                         _youtubePlayerController.play();
                         } else {
                         _controller.value.isPlaying
                             ?
                         _controller.pause()
                             :
                         _controller.play();
                         }
                         },
                          child: _isYouTube
                              ?
                          Icon(_isVideo
                         ?
                          Icons.pause
                              :
                          LineIcons.play,
                           size: 30.0,)
                          :
                            Icon(_controller.value.isPlaying
                          ?
                            Icons.pause
                                :
                            LineIcons.play,
                        size: 30.0,)
                        ),
                    )
                   )
                 ]
               )
            ]
            )
           ]
          )
         );


 @override
 void dispose() {
   _isYouTube
       ?
   _youtubePlayerController.dispose()
       :
   _controller.dispose();
   super.dispose();
 }
}