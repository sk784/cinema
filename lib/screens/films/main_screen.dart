import 'dart:async';

import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/films/film.dart';
import 'package:cinema/model/response/films/new_film.dart';
import 'package:cinema/screens/films/film_screen.dart';
import 'package:cinema/widgets/new_films_item.dart';
import 'package:cinema/widgets/price_item.dart';
import 'package:cinema/widgets/home_page_video_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:intl/intl.dart';
import 'package:line_icons/line_icons.dart';

import '../../main.dart';


class MainScreen extends StatefulWidget {
  MainScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>{

  var _value = 1;
  int _filmIndex = 0;
  var _date = DateTime.now();
  String _filmDate = "Скоро";
  var service = MovieApi.instance;

  Future<List<Film>> getMoviesListByDay;
  Future<List<NewFilm>> getHighlightsMovies;


  @override
  void initState() {
    super.initState();
      getMoviesListByDay = service.getMoviesListByDay(int.parse(DateFormat("yyMMdd").format(_date)));
      getHighlightsMovies = service.getHighlightsMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
        body: RefreshIndicator(
          onRefresh: _handleRefresh,
          child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(left: 16.0, top: 74.0, bottom: 10.0,
                      right: 16.0),
                      child: Text(
                        'Кинотеатр "Изумруд"',
                        style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold
                        ),
                      )
                  ),
                Theme(
                 data: Theme.of(context).copyWith(
                    canvasColor: Colors.blue
                 ),
                   child: Container(
                      child: DropdownButtonHideUnderline(
                        child: _dropDownButtonList(),
                      ),
                      margin: const EdgeInsets.only(left: 16.0, right: 16.0,
                          bottom: 20.0),
                      padding: const EdgeInsets.only(left: 16.0, right: 16.0,
                          bottom: 8.0,top: 8.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                        color: Colors.blue,
                        border: Border.all(color: Colors.blue)),
                  )
               ),
                      _buildFilmsMainPages(),
                      Container(
                          alignment: Alignment.topLeft,
                          margin: const EdgeInsets.only(left: 16.0,top: 28.0),
                          child: Text('Новинки',
                            style: TextStyle(
                                fontSize: 24.0,
                                fontWeight: FontWeight.bold
                            ),
                          )
                      ),
                      _buildNewFilmsList(),
                    logOut(),
                    ],
                  )
          ),
        )
    );
  }

  Widget logOut() =>
      Container(
          child : FlatButton(
              onPressed: clear,
              child:  Text(
                'Разлогиниться',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                    color: Colors.blue
                ),
              ))
      );

  Future<void> clear() async {
    await service.setMobileToken('');
    RestartWidget.restartApp(context);
  }

  DropdownButton _dropDownButtonList() => DropdownButton<int>(
    items: [
      DropdownMenuItem<int>(
        value: 1,
        child: Text(
          "Сегодня в кино",
        ),
      ),
      DropdownMenuItem<int>(
        value: 2,
        child: Text(
          "Завтра",
        ),
      ),
      DropdownMenuItem<int>(
        value: 3,
        child: Text(
          _filmDate,
        ),
      ),
    ],
    onChanged: (value) {
      setState(() {
        _value = value;
        if (value == 3) {
          _showDatePicker();
        } else {
          _date = DateTime.now().add(Duration(days: value - 1));
          getMoviesListByDay = service.getMoviesListByDay(int.parse(DateFormat("yyMMdd").format(_date)));
        }
        print(int.parse(DateFormat("yyMMdd").format(_date)));

      });
    },
    value: _value,
    isDense: true,
    isExpanded: true,
    style: TextStyle(
      color: Colors.white,
    ),
    icon: Icon(Icons.keyboard_arrow_down,
             color: Colors.white,),
  );

  void _showDatePicker() async {
      DateTime newDateTime = await showRoundedDatePicker(
        context: context,
        locale: Locale("ru", "RU"),
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 100)),
        theme: ThemeData(primarySwatch: Colors.blue),
      );
      if (newDateTime != null) {
        setState(() {
          _date = newDateTime;
          _filmDate = DateFormat("dd.MM.yyyy").format(_date);
          getMoviesListByDay = service.getMoviesListByDay(int.parse(DateFormat("yyMMdd").format(_date)));
        });
      }
  }

  Widget _buildFilmsMainPages() =>
      FutureBuilder(
        future: getMoviesListByDay,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data != null && snapshot.data.toString()!="[]" ) {
            final List<Film> films = snapshot.data;
           return Column(
               children:<Widget> [
                 SizedBox(
                  height: 335.0,
                   child: PageView.builder(
                     pageSnapping: true,
                     itemCount: films.length,
                     onPageChanged: (int index) {
                     setState(() {
                       _filmIndex = index;
                      });
                     },
                     itemBuilder: (context, index) {
                        return Column(
                         children: <Widget>[
                           GestureDetector(
                             onTap: () =>
                                 Navigator.push(context,
                                   MaterialPageRoute(builder: (context) =>
                                       FilmScreen(
                                           id:
                                           films[index].id,
                                           title:
                                           films[index].title
                                               ??
                                               "",
                                           restriction:
                                           films[index].age
                                               ??
                                               "",
                                           duration:
                                           films[index].duration
                                               ??
                                               0,
                                           genre:
                                           films[index].genre
                                               ??
                                               "",
                                           synopsis:
                                           films[index].longDesc
                                               ??
                                               "",
                                           urlPhoto:
                                           films[index].media!=null
                                               ?
                                           films[index].media.elementAt(0)
                                               :
                                           "",
                                           urlVideo:
                                           films[index].media!=null
                                               ?
                                           films[index].media.elementAt(1)
                                               :
                                           ""
                                       )
                                   ),
                                 ),
                             child: HomePageVideoWidget(
                                  title: films[index].title
                                      ??
                                      "",
                                  urlPhoto: films[index].media!=null
                                      ?
                                  films[index].media.elementAt(0)
                                      :
                                  "",
                                  urlVideo: snapshot.data[index].media!=null
                                      ?
                                  films[index].media.elementAt(1)
                                      :
                                  "",
                                  restriction: films[index].age
                                      ??
                                      "",
                                ),
                           ),
                         Container(
                          alignment: Alignment.topLeft,
                             margin: const EdgeInsets.only(left: 16.0, top: 12.0, bottom: 12.0,
                                 right: 16.0),
                           child: Text(films[index].shortDesc
                               ??
                               "",
                             style: TextStyle(
                               fontSize: 12.0,
                           ),
                         )
                         ),
                         Container(
                          height: 52.0,
                          alignment: Alignment.topLeft,
                          margin: const EdgeInsets.only(left: 16.0),
                          child: ListView.builder(
                           scrollDirection: Axis.horizontal,
                            itemCount: films[index].prices.length,
                           shrinkWrap: true,
                           itemBuilder: (BuildContext context, int listIndex) {
                            return Container(
                              child: PriceItem(
                                films[index].prices.elementAt(listIndex).time,
                                films[index].prices.elementAt(listIndex).price,
                                  (DateFormat("yyMMdd").format(DateTime.now()) == DateFormat("yyMMdd").format(_date))&&
                                      (films[index].prices.elementAt(listIndex).time
                                          < int.parse(DateFormat("HH:mm").format(DateTime.now()).replaceAll(":", "")))
                                      ?
                                  false
                                      :
                                  true,

                              )
                          );
                         },
                         ),
                         ),
                  ]
                );
                }
               ),
              ),
                 Container(
                   height: 8.0,
                   alignment: Alignment.center,
                   margin: const EdgeInsets.symmetric(horizontal: 16.0),
                   child: ListView.builder(
                     scrollDirection: Axis.horizontal,
                     itemCount: films.length,
                     shrinkWrap: true,
                     itemBuilder: (BuildContext context, int index) {
                       return _filmIndex==index
                           ?
                       Icon(Icons.brightness_1,
                       color: Color(0xFFC4C4C4),size: 12.0,)
                           :
                       Icon(LineIcons.circle,color: Color(0xFFC4C4C4),size: 12.0,);
                     },
                   ),
                 ),
               ],
             );
          } else if (snapshot.data.toString()=="[]"){
            return Container(
                child: Center(
                  child: Text(
                    'На этот день нет фильмов в расписании!',
                    style: TextStyle(
                        fontSize: 16.0,
                        color:  Color(0xFF8A8A8A),
                        fontWeight: FontWeight.bold
                    ),
                  ),
                )
            );
          } else if (snapshot.hasError) {
                 return Padding(
                    padding: const EdgeInsets.only(top: 16.0,left: 16.0),
                    child: Text('Ошибка при загрузке данных: ${snapshot.error}'),
                  );
          } else {
            return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                )
            );
          }
        }
      );


  Future<Null> _handleRefresh() async {
    await Future.delayed(Duration(seconds: 3));

    setState(() {

    });

    return null;
  }


  Widget _buildNewFilmsList() =>
      FutureBuilder(
        future: getHighlightsMovies,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data != null && snapshot.data.toString()!="[]" ) {
          final List<NewFilm> newFilms = snapshot.data;
          return
          ListView.separated(
           itemCount: newFilms.length,
            separatorBuilder: (context, index) => SizedBox(
              height: 16.0,
            ),
            padding: const EdgeInsets.only(left: 16.0,right: 16.0,top: 20.0),
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: NewFilmsItem(
                  newFilms[index].media!=null
                        ?
                  newFilms[index].media.elementAt(0)
                        :
                    '',
                  newFilms[index].title
                        ??
                        "",
                  newFilms[index].genre
                        ??
                        "",
                  newFilms[index].country
                        ??
                        "",
                  newFilms[index].minPrice
                        ??
                        0 ,
                  newFilms[index].maxPrice
                        ??
                        0,
                  newFilms[index].age
                        ??
                        "",
                ),
                onTap:
                    () =>
                    Navigator.push(context,
                      MaterialPageRoute(builder: (context) =>
                          FilmScreen(
                              id:
                              newFilms[index].id,
                              title:
                              newFilms[index].title
                                  ??
                                  "",
                              restriction:
                              newFilms[index].age
                                  ??
                                  "",
                              duration:
                              newFilms[index].duration
                                  ??
                                  0,
                              genre:
                              newFilms[index].genre
                                  ??
                                  "",
                              synopsis:
                              newFilms[index].longDesc
                                  ??
                                  "",
                              urlPhoto:
                              newFilms[index].media!=null
                                  ?
                              newFilms[index].media.elementAt(0)
                                  :
                              '',
                              urlVideo:
                              newFilms[index].media!=null
                                  ?
                              newFilms[index].media.elementAt(1)
                                  :
                              ""
                          )
                      ),
                    ),
              );
            },
         );
        } else if (snapshot.data.toString()=="[]"){
          return Container(
              padding: const EdgeInsets.only(top: 16.0),
              child: Center(
                child: Text(
                  'На данный момент нет новинок',
                  style: TextStyle(
                      fontSize: 16.0,
                      color:  Color(0xFF8A8A8A),
                      fontWeight: FontWeight.bold
                  ),
                ),
              )
          );
        } else if (snapshot.hasError) {
          return Padding(
            padding: const EdgeInsets.only(top: 16.0,left: 16.0),
            child: Text('Ошибка при загрузке данных: ${snapshot.error}'),
          );
        } else {
          return Container(
              child: Center(
                child: CircularProgressIndicator(),
              )
          );
        }
        }
      );

}