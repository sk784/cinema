import 'dart:convert';
import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/account/recovery.dart';
import 'package:cinema/widgets/countdown_timer.dart';
import 'package:cinema/utils/phone_form_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class RestorePassword extends StatefulWidget {
  @override
  _RestorePassword createState() => _RestorePassword();
}

class _RestorePassword extends State<RestorePassword> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final RusNumberTextInputFormatter _phoneNumberFormatter =
  RusNumberTextInputFormatter();
  TextEditingController _controller = TextEditingController();
  bool _checkCode = false;
  bool _resendCode = false;
  bool _isLoading = false;
  bool _getPassword = false;
  String _phone = "";
  bool _wrongData = false;
  Recovery _recoveryResponse;
  String _token;
  var service = MovieApi.instance;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),
        ),
        key: _scaffoldKey,
        body: _isLoading
            ?
        Center(
          child: CircularProgressIndicator(),
              )
            :
        SingleChildScrollView(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(
                height: 60.0,
              ),
              Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(left: 16.0, bottom: 24.0),
                  child: Text(
                    'Восстановить пароль',
                    style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold
                    ),
                  )),
              Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(left: 16.0, bottom: 16.0),
                  child: Text(_checkCode == false
                      ?
                  'Введите телефон, который был указан при регистрации'
                      :
                  'На Ваш телефон отправлен код, введите его '
                      'для сброса пароля',
                  )),
              Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(left: 16.0, bottom: 10.0),
                  child: Text(
                    _checkCode == false
                        ?
                    'Телефон'
                        :
                    'Код для сброса пароля',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0,
                    ),
                  )),
              buildPhoneTextField(),
              Visibility(
                  visible: _checkCode == true,
                  child: FlatButton(
                      onPressed: _resendCodePressed,
                      child: Text(
                        'Отправить код повторно',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.blue
                        ),
                      )
                  )
              ),
              Visibility(
                  visible: _resendCode == true,
                  child: CountDownTimer(
                      secondsRemaining: 180,
                      whenTimeExpires: () {
                        setState(() {
                          _resendCode = false;
                        });
                      })
              ),
              confirmButton(),
              _recoveryResponse!=null
                  ?
              Visibility(
                  visible: _getPassword == true,
                  child: Container(
                    margin: const EdgeInsets.only(top:16.0, left: 16.0, right: 16.0),
                    child: Text("Ваш новый пароль: ${_recoveryResponse.password}"
                        ),
                  )
              )
                  :
              Container(),
            ],
          )
    )
    );
  }

  Widget buildPhoneTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          controller: _controller,
          inputFormatters: _checkCode == false
              ?
          <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
            _phoneNumberFormatter
          ]
              :
          null,
          keyboardType: TextInputType.phone,
          decoration: _checkCode == false
              ?
          InputDecoration(
              prefixText: '+7',
              prefixStyle: TextStyle(color: Theme.of(context).brightness
                  == Brightness.dark
                  ? Colors.white:Colors.black,
                  fontSize: 16.0)
          )
              :
          null,
        ),
      );

  Widget confirmButton() =>
      Padding(padding: EdgeInsets.only(left: 16.0,top:90.0,right: 16.0),
          child: SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed: _resendCode == true
                    ?
                null
                    :
                    () {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  _checkCode == false
                      ?
                  'Отправить'
                      :
                  'Подтвердить',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )),
          ));


  void _confirmPressed() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    if (_checkCode == false) {
      _phone = "+7" + _controller.text.replaceAll("(", "").
      replaceAll(")", "").replaceAll("-", "");

      service.userRequestCode(_phone).then((response) {
        if (response.statusCode == 202) {
          setState(() {
            _isLoading = false;
            _wrongData = false;
            _checkCode = !_checkCode;
            _controller.clear();
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Успешно, код отправлен"),
            backgroundColor: Colors.green,
          ));
          print(response.body);
        }
        else if (response.statusCode == 404) {
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Аккаунт не найден"),
            backgroundColor: Colors.red,
          ));
          print(response.body);
          print(response.statusCode);
        }
        else if (response.statusCode == 429) {
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("С момента предыдущей отправки кода на этот телефон "
                "не прошло 3 минут"),
            backgroundColor: Colors.red,
          ));
          print(response.body);
        }
        else if (response.statusCode == 450) {
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("На этот номер отправка невозможна"),
            backgroundColor: Colors.red,
          ));
          print(response.body);
        }
        else {
          print(response.statusCode);
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
        }
      }).catchError((error) {
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неизвестная ошибка"),
          backgroundColor: Colors.red,
        ));
        print('error : $error');
      });
    }
    else {
      void _processResponse(http.Response response) {
        if (response.statusCode == 200) {
          setState(() {
            _isLoading = false;
            _wrongData = false;
            _controller.clear();
            _recoveryResponse = Recovery.fromJson(json.decode(response.body));
            _token = _recoveryResponse.token;
             print(_token);
             print(response.body);
             service.setMobileToken(_token);
            _getPassword=true;
          });

        }
        else if (response.statusCode == 404) {
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Аккаунт не найден"),
            backgroundColor: Colors.red,
          ));
          print(response.body);
          print(response.statusCode);
        }
        else if (response.statusCode == 410) {
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Неверный код и превышен лимит попыток; "
                "запрос на восстановление удалён"),
            backgroundColor: Colors.red,
          ));
          print(response.body);
        }
        else {
          print(response.statusCode);
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
        }
      }
      service.userVerifyCode(_phone, _controller.text).then((_processResponse))
          .catchError((error) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неизвестная ошибка"),
          backgroundColor: Colors.red,
        ));
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        print('error : $error');
      });
    }
  }

  void _resendCodePressed() {
    setState(() {
      _resendCode = !_resendCode;
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}




