import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/account/user.dart';
import 'package:cinema/screens/auth/registration.dart';
import 'package:cinema/screens/auth/restore_password.dart';
import 'package:cinema/utils/phone_form_format.dart';
import 'package:cinema/tabs_screen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:device_info/device_info.dart';
import 'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;

class SignIn extends StatefulWidget {

  @override
 _SignInState createState() => _SignInState();
 }

 class _SignInState extends State<SignIn> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final RusNumberTextInputFormatter _phoneNumberFormatter =
  RusNumberTextInputFormatter();

  bool _wrongData = false;
  bool _obscureText = true;
  bool _isLoading = false;
  String _token = "";
  String _deviceId = "";
  bool _autoValidatePhone = false;
  bool _autoValidatePass = false;
  TextEditingController _tecPhone = TextEditingController();
  TextEditingController _tecPass = TextEditingController();
  User _userResponse;
  FocusNode _focusNode;
  var service = MovieApi.instance;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  //  fireBaseCloudMessagingListeners();
    initPlatformState();
    _tecPhone.addListener(() => setState(() {
      _autoValidatePhone = _tecPhone.text.length > 12
          ?
      true
          :
      false;
    }));
    _tecPass.addListener(() => setState(() {
      _autoValidatePass = _tecPass.text.length > 0
          ?
      true
          :
      false;
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body:_isLoading
          ?
      Center(
        child: CircularProgressIndicator(),
      )
          :
      SingleChildScrollView(
       child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 150.0,
            ),
            Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 16.0,bottom: 24.0),
                child : Text(
                  'Вход в аккаунт',
                  style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                   //   color: Colors.white,
                  ),
                )),
            Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 16.0,bottom: 10.0),
                child : Text(
                  'Телефон',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0,
                  ),
                )),
            buildPhoneTextField(),
            Container(
                alignment: Alignment.topLeft,
                margin: const EdgeInsets.only(left: 16.0,bottom: 10.0),
                child : Text(
                  'Пароль',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0,
                  ),
                )),
            buildPasswordTextField(context),
            forgetPasswordButton(),
            confirmButton(),
            registrationButton()
          ],
        )
    )
    );
  }

  Widget buildPhoneTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          controller: _tecPhone,
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(_focusNode);
          },
          inputFormatters: <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
            _phoneNumberFormatter
          ],
          keyboardType: TextInputType.phone,
          decoration:  InputDecoration(
            prefixText: '+7',
            prefixStyle: TextStyle(color: Theme.of(context).brightness ==
                Brightness.dark
                ?
            Colors.white
                :
            Colors.black,
                fontSize: 16.0)
          ),
        ),
      );

  Widget buildPasswordTextField(BuildContext context) =>
       Container(
         height: 46.0,
         margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 12.0),
         padding: const EdgeInsets.symmetric(horizontal: 12.0),
         decoration:
         BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
             border: Border.all(color: _wrongData==false
                 ?
             Color(0xFFE1E1E1)
                 :
             Colors.red)
         ),
        child: TextFormField(
          controller: _tecPass,
          focusNode: _focusNode,
          obscureText: _obscureText,
          decoration: InputDecoration(
            suffixIcon: GestureDetector(
              dragStartBehavior: DragStartBehavior.down,
              onTap: (){
                setState(() {
                  _obscureText =!_obscureText;
                });
              },
              child: Icon(
                _obscureText
                    ?
                Icons.visibility
                    :
                Icons.visibility_off,
              ),
            )
          ),
        ),
      );


  Widget forgetPasswordButton() =>
      Container(
          alignment: Alignment.topRight,
          child : FlatButton(
           onPressed: _forgetPasswordPressed,
           child:  Text(
            'Забыли пароль?',
             style: TextStyle(
                 fontWeight: FontWeight.bold,
                 color: Colors.blue
             ),
          ))
      );

  Widget confirmButton() =>
       Padding(padding: EdgeInsets.symmetric(horizontal: 16.0),
          child:  SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed:  _autoValidatePhone ==false || _autoValidatePass==false
                ?
                null
                    :
                    () {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  'Войти',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                  ),
                )
            ),
          )
      );

  Widget registrationButton() =>
      FlatButton(
          onPressed: _registrationPressed,
          child:  Text(
            'Регистрация',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.blue
            ),
          ));


  void _forgetPasswordPressed() {
//    final target = Theme.of(context).brightness == Brightness.dark
//        ? Brightness.light
//        : Brightness.dark;
//    DynamicTheme.of(context).setBrightness(target);

    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RestorePassword()));
  }

  void _confirmPressed(){
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

      void _processResponse(http.Response response) {
        if(response.statusCode == 200){
          print(response.body);
          setState(() {
            _wrongData = false;
            _isLoading = false;
            _userResponse = User.fromJson(json.decode(response.body));
            service.setMobileToken( _userResponse.token);
          });
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => TabsScreen()),
                (Route<dynamic> route) => false,
          );
        }
        else if (response.statusCode == 401){
          print(response.statusCode);
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Вы ввели неверный логин или пароль"),
            backgroundColor:Colors.red,
          ));
        }
        else {
          print(response.statusCode);
          setState(() {
            _isLoading = false;
            _wrongData = true;
          });
        }
      }

    service.userLogin("+7"+_tecPhone.text.replaceAll("(", "").
    replaceAll(")", "").replaceAll("-", ""),_tecPass.text,_deviceId,_token)
        .then((_processResponse))
        .catchError((error){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Неизвестная ошибка"),
        backgroundColor:Colors.red,
      ));
      setState(() {
        _isLoading = false;
        _wrongData = true;
      });
      print('error : $error');
    });
  }

  void _registrationPressed() {
//    final target = Theme.of(context).brightness == Brightness.dark
//        ? Brightness.light
//        : Brightness.dark;
//
//    DynamicTheme.of(context).setBrightness(target);

    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Registration()));
  }

  Future<void> initPlatformState() async {
    try {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        print('Running on ${androidInfo.model}');
        _deviceId = androidInfo.model;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        print('Running on ${iosInfo.utsname.machine}');
        _deviceId = iosInfo.utsname.machine;
      }
    } on PlatformException {
      print( 'Error:Failed to get platform version.');
    }
    if (!mounted) return;
  }

//  void fireBaseCloudMessagingListeners() {
//    FirebaseAuth.instance.currentUser().then((user){
//      final FirebaseMessaging _fireBaseMessaging = FirebaseMessaging();
//      _fireBaseMessaging.getToken().then((token) {
//        print(token);
//        _token = token;
//      });
//    });
// }

  @override
  void dispose() {
    _tecPhone.dispose();
    _tecPass.dispose();
    _focusNode.dispose();
    super.dispose();
  }
 }




