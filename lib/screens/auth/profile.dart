import 'dart:convert';
import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/entity/account/notifications.dart';
import 'package:cinema/model/entity/account/update_profile.dart';
import 'package:cinema/widgets/settings_toggle.dart';
import 'package:cinema/tabs_screen.dart';
import 'package:flutter/material.dart';


class AuthProfile extends StatefulWidget {

  final String phone;
  const AuthProfile({Key key, this.phone}) : super(key: key);

  @override
  _AuthProfile createState() => _AuthProfile();
}

class _AuthProfile extends State<AuthProfile> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _wrongData = false;
  bool _newPasswordText = false;
  bool _notifications = true;
  bool _isLoading = false;
  bool _autoValidateName = false;
  bool _autoValidateOldPass = false;
  TextEditingController _tecName = TextEditingController();
  TextEditingController _tecOldPass = TextEditingController();
  TextEditingController _tecNewPass = TextEditingController();
  FocusNode _oldPassFocusNode;
  FocusNode _newPassFocusNode;
  var service = MovieApi.instance;

  @override
  void initState() {
    super.initState();
    _oldPassFocusNode = FocusNode();
    _newPassFocusNode = FocusNode();
    _tecName.addListener(() => setState(() {
      _autoValidateName = _tecName.text.length > 0
          ?
      true
          :
      false;
    })
    );

    _tecOldPass.addListener(() => setState(() {
      _autoValidateOldPass = _tecOldPass.text.length > 0
          ?
      true
          :
      false;
    }));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Theme
              .of(context)
              .scaffoldBackgroundColor,
          brightness: Theme
              .of(context)
              .brightness,
          iconTheme: Theme
              .of(context)
              .iconTheme
              .copyWith(color: Theme
              .of(context)
              .accentColor),
        ),
        body: _isLoading
            ?
        Center(
          child: CircularProgressIndicator(),
             )
            :
        SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 24.0,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, bottom: 24.0),
                    child: Text(
                      'Профиль',
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                        //   color: Colors.white,
                      ),
                    )
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, bottom: 10.0),
                    child: Text(
                      'Имя',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )
                ),
                buildNameTextField(),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,
                        bottom: 10.0),
                    child: Text(
                      'Пароль',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )
                ),
                buildPasswordTextField(context),
                Visibility(
                  visible: _newPasswordText == true,
                  child: Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(left: 16.0,
                          bottom: 6.0, top: 10.0),
                      child: Text(
                        'Новый пароль',
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      )
                  ),
                ),
                buildNewPasswordTextField(context),
                confirmButton(),
                SettingsToggle(
                  title: 'Уведомления из приложения',
                  onChanged: (bool isOn) {
                    setState(() {
                      _notifications = isOn;
                      isOn =!isOn;
                    });
                  },
                  value: _notifications,
                ),
              ],
            )
        )
    );
  }

  Widget buildNameTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          controller: _tecName,
          keyboardType: TextInputType.text,
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(_oldPassFocusNode);
          },
        ),
      );

  Widget buildPasswordTextField(BuildContext context) =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 10.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          obscureText: true,
          controller: _tecOldPass,
          focusNode: _oldPassFocusNode,
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(_newPassFocusNode);
          },
        ),
      );

  Widget buildNewPasswordTextField(BuildContext context) =>
      Visibility(
        visible: _newPasswordText == true,
          child:
           Container(
             height: 46.0,
             margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 34.0,),
             padding: const EdgeInsets.symmetric(horizontal: 12.0),
            decoration:
            BoxDecoration(
             borderRadius: BorderRadius.all(const Radius.circular(5.0)),
                border: Border.all(color: _wrongData==false
                    ?
                Color(0xFFE1E1E1)
                    :
                Colors.red)
            ),
             child: TextFormField(
               controller: _tecNewPass,
               obscureText: true,
               focusNode: _newPassFocusNode,
        ),
      )
      );

  Widget confirmButton() =>
      Padding(padding: EdgeInsets.only(left: 16.0, right: 16.0,bottom: 32.0,
          top: 270.0),
          child: SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed:_autoValidateName ==false||_autoValidateOldPass==false
                    ?
                null
                    :
                    () {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  'Подтвердить',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
            ),
          )
      );


  void _confirmPressed() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    UpdateProfile updateProfile = UpdateProfile(_tecName.text, _tecOldPass.text,
        _newPasswordText==true?_tecNewPass.text:_tecOldPass.text,
        null, null, null, _notifications==true
            ?
        Notifications(1,1)
            :
        Notifications(0,0));
    String jsonUpdateProfile = jsonEncode(updateProfile);
    print(jsonUpdateProfile);

     service.userUpdate(jsonUpdateProfile).then((response){
      if(response.statusCode == 200){
        setState(() {
          _isLoading = false;
          _wrongData = false;
        });
        print(response.statusCode);
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => TabsScreen()),
              (Route<dynamic> route) => false,
        );
      }
      else if (response.statusCode == 401){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Ошибка авторизации"),
          backgroundColor:Colors.red,
        ));
        print(response.statusCode);
      }
      else if (response.statusCode == 403){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неверный старый пароль"),
          backgroundColor:Colors.red,
        ));
        print(response.statusCode);
      }
      else {
        print(response.statusCode);
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
      }
    }).catchError((error){
       _scaffoldKey.currentState.showSnackBar(SnackBar(
         content: Text("Неизвестная ошибка"),
         backgroundColor:Colors.red,
       ));
       setState(() {
         _isLoading = false;
         _wrongData = true;
       });
      print('error : $error');
    });
  }

  @override
  void dispose() {
    _tecName.dispose();
    _tecOldPass.dispose();
    _tecNewPass.dispose();
    _oldPassFocusNode.dispose();
    _newPassFocusNode.dispose();
    super.dispose();
  }
}