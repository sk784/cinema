import 'dart:async';
import 'dart:convert';
import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/application/legal.dart';
import 'package:cinema/screens/auth/input_code.dart';
import 'package:cinema/widgets/html_page.dart';
import 'package:cinema/utils/phone_form_format.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class Registration extends StatefulWidget {
  @override
  _Registration createState() => _Registration();
}

class _Registration extends State<Registration> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final RusNumberTextInputFormatter _phoneNumberFormatter =
  RusNumberTextInputFormatter();
  bool _wrongData = false;
  bool _obscureText = true;
  bool _isLoading = false;
  bool _autoValidatePhone = false;
  bool _autoValidatePass = false;
  String _phone = "";
  TextEditingController _tecPhone = TextEditingController();
  TextEditingController _tecPass = TextEditingController();
  TapGestureRecognizer _recognizer;
  Legal _legalResponse;
  String _url;
  FocusNode _focusNode;
  var service = MovieApi.instance;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    _tecPhone.addListener(() => setState(() {
      _autoValidatePhone = _tecPhone.text.length > 12
          ?
      true
          :
      false;
    }));
    _tecPass.addListener(() => setState(() {
      _autoValidatePass = _tecPass.text.length > 0
          ?
      true
          :
      false;
    }));
    _recognizer = TapGestureRecognizer()
      ..onTap = () {
      _launchURL();
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),
        ),
        body: _isLoading
            ?
        Center(
          child: CircularProgressIndicator(),
        )
            :
        SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(
                  height: 60.0,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,bottom: 24.0),
                    child : Text(
                      'Регистрация',
                      style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold,
                        //   color: Colors.white,
                      ),
                    )),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,bottom: 10.0),
                    child : Text(
                      'Введите Ваш телефон',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )),
                buildPhoneTextField(),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0,
                        bottom: 10.0),
                    child : Text(
                      'Придумайте пароль',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                      ),
                    )),
                buildPasswordTextField(context),
                confirmButton(),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
                  child : RichText(
                    text: TextSpan(
                      text: 'Нажимая кнопку "Зарегистрироваться", '
                          'Вы принимаете ',
                      style: TextStyle(color: Theme.of(context).brightness ==
                          Brightness.dark
                          ?
                      Colors.white
                          :
                      Colors.black) ,
                      children: <TextSpan>[
                        TextSpan(text: 'условия пользовательского '
                            'соглашения',
                            style: TextStyle(decoration: TextDecoration.underline,
                                color: Colors.blue),
                            recognizer: _recognizer),
                      ],
                    ),
                  ),
                ),
              ],
            )
        )
    );
  }

  Widget buildPhoneTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          controller: _tecPhone,
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(_focusNode);
          },
          inputFormatters: <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
            _phoneNumberFormatter
          ],
          keyboardType: TextInputType.phone,
          decoration:  InputDecoration(
              prefixText: '+7',
              prefixStyle: TextStyle(color: Theme.of(context).brightness ==
                  Brightness.dark
                  ?
              Colors.white
                  :
              Colors.black,
                  fontSize: 16.0)
          ),
        ),
      );

  Widget buildPasswordTextField(BuildContext context) =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          focusNode: _focusNode,
          controller: _tecPass,
          obscureText: _obscureText,
          decoration: InputDecoration(
              suffixIcon: GestureDetector(
                dragStartBehavior: DragStartBehavior.down,
                onTap: (){
                  setState(() {
                    _obscureText =!_obscureText;
                  });
                },
                child: Icon(
                  _obscureText
                      ?
                  Icons.visibility
                      :
                  Icons.visibility_off,
                ),
              )
          ),
        ),
      );


  Widget confirmButton() =>
       Padding(padding: EdgeInsets.symmetric(horizontal: 16.0),
          child:  SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed: _autoValidatePhone ==false || _autoValidatePass==false
                    ?
                null
                    :
                    () {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  'Зарегистрироваться',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )),
          ));


  void _confirmPressed() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
    _phone = "+7"+_tecPhone.text;

    service.userRegistration("+7"+_tecPhone.text.replaceAll("(", "").
    replaceAll(")", "").replaceAll("-", ""), _tecPass.text).then((response){
      if(response.statusCode == 202){
        setState(() {
          _isLoading = false;
          _wrongData = false;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
           content: Text("Успешно, SMS отправлено"),
           backgroundColor:Colors.green,
        ));
        print(response.body);

        Timer(Duration(seconds: 1), () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) =>
                  InputCode(phone: _phone)));
        });
      }
      else if (response.statusCode == 409){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Телефон уже зарегистрирован"),
          backgroundColor:Colors.red,
        ));
        print(response.body);
      }
      else if (response.statusCode == 429){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("С момента предыдущей отправки кода на этот телефон "
              "не прошло 3 минут"),
          backgroundColor:Colors.red,
        ));
        print(response.body);
      }
      else if (response.statusCode == 450){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("На этот номер отправка невозможна"),
          backgroundColor:Colors.red,
        ));
        print(response.body);
      }
      else {
        print(response.statusCode);
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
      }
    }).catchError((error){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Неизвестная ошибка"),
        backgroundColor:Colors.red,
      ));
      setState(() {
        _isLoading = false;
        _wrongData = true;
      });
      print('error: $error');
    });
  }

  void _urlResponse(http.Response response) {
     if(response.statusCode == 200){
      _legalResponse = Legal.fromJson(json.decode(response.body));
      _url = _legalResponse.privacy;
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              HTMLPage(_url)));
    }
    else {
      print(response.statusCode);
    }
  }

  _launchURL() {
     service.legalTexts().then((_urlResponse))
        .catchError((error){
      print('error : $error');
    });
  }

  @override
  void dispose() {
    _tecPhone.dispose();
    _tecPass.dispose();
    _focusNode.dispose();
    super.dispose();
  }
}