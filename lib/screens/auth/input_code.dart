import 'dart:convert';
import 'package:cinema/model/api_client.dart';
import 'package:cinema/model/response/account/code.dart';
import 'package:cinema/screens/auth/profile.dart';
import 'package:cinema/widgets/countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class InputCode extends StatefulWidget {
  final String phone;
  const InputCode({Key key, this.phone}) : super(key: key);
  @override
  _InputCode createState() => _InputCode();
}

class _InputCode extends State<InputCode> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _wrongData = false;
  bool _resendCode = false;
  bool _isLoading = false;
  TextEditingController _tecCode = TextEditingController();
  Code _codeResponse;
  String _token;
  var service = MovieApi.instance;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),
        ),
        key: _scaffoldKey,
        body:  _isLoading
            ?
        Center(
         child: CircularProgressIndicator(),
               )
            :
        SingleChildScrollView(
                  child:Column(
                   crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                  SizedBox(
                    height: 60.0,
                  ),
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(left: 16.0, bottom: 24.0),
                      child: Text(
                        'Введите код',
                        style: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold
                        ),
                      )
                  ),
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(left: 16.0, right: 16.0,
                          bottom: 16.0),
                      child: Text(
                           'Мы отправили код активации по SMS на \nномер'
                               ' ${widget.phone.replaceRange(4, 13, "*******")})',
                      )
                  ),
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(left: 16.0, bottom: 10.0),
                      child: Text('Код активации',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12.0,
                        ),
                      )
                  ),
                  buildCodeTextField(),
                  FlatButton(
                      onPressed: _resendCodePressed,
                          child: Text(
                            'Отправить код повторно',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue
                            ),
                          )
                  ),
                  Visibility(
                      visible: _resendCode == true,
                      child: CountDownTimer(
                        secondsRemaining: 180,
                          whenTimeExpires: () {
                            setState(() {
                              _resendCode = false;
                            });
                          })
                  ),
                  confirmButton(),
                  ],
                )
        )
    );
  }

  Widget buildCodeTextField() =>
      Container(
        height: 46.0,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        decoration:
        BoxDecoration(
            borderRadius: BorderRadius.all(const Radius.circular(5.0)),
            border: Border.all(color: _wrongData==false
                ?
            Color(0xFFE1E1E1)
                :
            Colors.red)
        ),
        child: TextFormField(
          controller: _tecCode,
          keyboardType: TextInputType.number,
        ),
      );


  Widget confirmButton() =>
      Padding(padding: EdgeInsets.only(left: 16.0,top:90.0,right: 16.0),
          child: SizedBox(
            height: 46.0,
            child: RaisedButton(
                onPressed: _resendCode == true
                    ?
                null
                    :() {
                  setState(() {
                    _isLoading = true;
                  });
                  _confirmPressed();
                },
                color: Colors.blue,
                child: Text(
                  'Подтвердить',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                )
            ),
          )
      );


  void _confirmPressed() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    void _processResponse(http.Response response) {
      if(response.statusCode == 201){
        setState(() {
          _isLoading = false;
          _wrongData = false;
          _codeResponse = Code.fromJson(json.decode(response.body));
          _token = _codeResponse.token;
          print(_token);
          service.setMobileToken(_token);
          print(response.body);
          print(response.statusCode);
        });
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) =>
                AuthProfile(phone: widget.phone)));
      }
      else if (response.statusCode == 403){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неверный код"),
          backgroundColor:Colors.red,
        ));
        print(response.statusCode);
      }
      else if (response.statusCode == 404){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Аккаунт не найден"),
          backgroundColor:Colors.red,
        ));
        print(response.statusCode);
      }
      else if (response.statusCode == 410){
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Неверный код и превышен лимит попыток; аккаунт удалён"),
          backgroundColor:Colors.red,
        ));
        print(response.statusCode);
      }
      else {
        print(response.statusCode);
        setState(() {
          _isLoading = false;
          _wrongData = true;
        });
      }
    }
    service.userConfirmCode(widget.phone.replaceAll("(", "").
    replaceAll(")", "").replaceAll("-", ""), _tecCode.text).then((_processResponse))
        .catchError((error){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Неизвестная ошибка"),
        backgroundColor:Colors.red,
      ));
      setState(() {
        _isLoading = false;
        _wrongData = true;
      });
      print('error : $error');
    });
  }

  void _resendCodePressed() {
    setState(() {
      _resendCode =!_resendCode;
    });
  }

  @override
  void dispose() {
    _tecCode.dispose();
    super.dispose();
  }
}