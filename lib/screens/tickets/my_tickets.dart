import 'package:cinema/screens/tickets/ticket_screen.dart';
import 'package:cinema/widgets/ticket_card_item.dart';
import 'package:flutter/material.dart';

class MyTickets extends StatefulWidget {

  MyTickets({Key key,}) : super(key: key);

  @override
  _MyTickets createState() => _MyTickets();
}

class _MyTickets extends State<MyTickets>{

  final List<TicketScreen> items = List();

  @override
  void initState() {
    super.initState();
    setState(() {
      items.add(TicketScreen('Титаник','драма','США',"24 февраля в 19:00",
      2, 5, 8, 10868, 16));
      items.add(TicketScreen('Джентльмены удачи','комедия','Россия',
          "26 февраля в 21:00", 4, 9, 12, 10844, 0));
      items.add(TicketScreen('Титаник','драма','США',"24 февраля в 19:00",
          2, 5, 8, 10868, 16));
      items.add(TicketScreen('Джентльмены удачи','комедия','Россия',
          "26 февраля в 21:00", 4, 9, 12, 10844, 0));
      items.add(TicketScreen('Титаник','драма','США',"24 февраля в 19:00",
          2, 5, 8, 10868, 16));
      items.add(TicketScreen('Джентльмены удачи','комедия','Россия',
          "26 февраля в 21:00", 4, 9, 12, 10844, 0));
      items.add(TicketScreen('Титаник','драма','США',"24 февраля в 19:00",
          2, 5, 8, 10868, 16));
      items.add(TicketScreen('Джентльмены удачи','комедия','Россия',
          "26 февраля в 21:00", 4, 9, 12, 10844, 0));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          brightness: Theme.of(context).brightness,
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: Theme.of(context).accentColor),

                ),
        body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(left: 16.0, top: 24.0, bottom: 20.0),
                    child: Text(
                      'Мои билеты',
                      style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold
                      ),
                    )
                ),
                buildTickets(),
              ],
            )
        )
    );
  }

  Widget buildTickets() =>
//      FutureBuilder(
//        //future: ,
//        builder: (BuildContext context, AsyncSnapshot snapshot) {
//        if (snapshot.data != null) {
     //     return
           ListView.separated(
           //  itemCount: snapshot.data.length,
             physics: NeverScrollableScrollPhysics(),
             shrinkWrap: true,
             scrollDirection: Axis.vertical,
             itemCount: items.length,
             separatorBuilder: (BuildContext context, int index) =>
             const SizedBox(
               height: 12.0,
             ),
             itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: TicketCardItem(
                  items[index].name,
                  items[index].date,
                  items[index].ticketHall,
                  items[index].ticketRow,
                  items[index].ticketSeat,
//                    snapshot.data[index].name,
//                    snapshot.data[index].date,
//                    snapshot.data[index].ticketHall,
//                    snapshot.data[index].ticketRow,
//                    snapshot.data[index].ticketSeat,
               ),
                onTap: ()=> Navigator.push(context,
                    MaterialPageRoute(builder: (context) =>
                    TicketScreen(
                    items[index].name,
                    items[index].genre,
                    items[index].country,
                    items[index].date,
                    items[index].ticketHall,
                    items[index].ticketRow,
                    items[index].ticketSeat,
                    items[index].ticketNumber,
                    items[index].restriction)
               )
//                  snapshot.data[index].name,
//                  snapshot.data[index].genre,
//                  snapshot.data[index].country,
//                  snapshot.data[index].date,
//                  snapshot.data[index].ticketHall,
//                  snapshot.data[index].ticketRow,
//                  snapshot.data[index].ticketSeat,
//                  snapshot.data[index].ticketNumber,
//                  snapshot.data[index].restriction,
                ),
              );
            },
           );
//        } else {
//          return Container(
//            margin: const EdgeInsets.only(top: 180.0),
//           child: Center(
//             child: Text("У Вас нет билетов"),
//            ),
//          );
//        }
//       },
//      );

}