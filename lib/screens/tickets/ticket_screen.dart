import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class TicketScreen extends StatelessWidget {
  final String name;
  final String genre;
  final String country;
  final String date;
  final int ticketHall;
  final int ticketRow;
  final int ticketSeat;
  final int ticketNumber;
  final int restriction;

  TicketScreen(this.name,this.genre,this.country,this.date,
      this.ticketHall,this.ticketRow,this.ticketSeat,this.ticketNumber,
      this.restriction);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        brightness: Theme.of(context).brightness,
        iconTheme: Theme.of(context)
        .iconTheme
        .copyWith(color: Theme.of(context).accentColor),
      ),
       body:SingleChildScrollView(
        child: Column(
            children: <Widget>[
          Container (
              decoration:
              BoxDecoration(
                  borderRadius: BorderRadius.all(const Radius.circular(20.0)),
                  border: Border.all(color: Color(0xFFF6F6F6)),
                  color: Color(0xFFF6F6F6),
              ),
              margin: const EdgeInsets.only(top: 24.0,left: 32.0, right: 32.0,bottom: 17.0),
            child: Column(
              children: <Widget>[
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                       child: Container(
                          margin: const EdgeInsets.only(top: 34.0,left: 28.0),
                          child: Text(name,
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                                fontSize: 24.0,
                                fontWeight: FontWeight.bold
                            ),
                          )
                         )
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 34.0,
                              right: 28.0),
                          child: Text(restriction.toString()+'+',
                            style: TextStyle(
                                fontSize: 18.0,
                            ),
                          )
                      ),
                    ]
                ),
                Container(
                    margin: const EdgeInsets.only(left: 28.0,top: 2.0),
                    alignment: Alignment.topLeft,
                    child: Text(genre+" / "+country,
                      style: TextStyle(
                        fontSize: 12.0,
                      ),
                    )
                ),
                Container(
                    margin: const EdgeInsets.only(left: 28.0,top: 16.0),
                    alignment: Alignment.topLeft,
                    child: Text('Дата и время',
                    ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 28.0,top: 2.0,bottom: 16.0),
                  alignment: Alignment.topLeft,
                  child: Text(date.toString(),
                    style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold
                      ),
                  )
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 2.0),
                    child:Row(
                        children: <Widget>[
                          Container(
                              margin: const EdgeInsets.only(left: 28.0,right: 16.0),
                              child: Text('Зал')
                          ),
                          Text('Ряд'),
                          Container(
                            margin: const EdgeInsets.only(left: 16.0),
                            child: Text('Место'),
                          ),
                        ]
                    )
                ),
                Row(
                    children: <Widget>[
                      Container(
                          margin: const EdgeInsets.only(left: 28.0,right: 32.0),
                          child: Text(ticketHall.toString(),
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold
                            ),
                          )
                      ),
                      Container(
                          child: Text(ticketRow.toString(),
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold
                            ),
                          )
                      ),
                      Container(
                          margin: const EdgeInsets.only(left: 32.0),
                          child: Text(ticketSeat.toString(),
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold
                            ),
                          )
                      ),
                    ]
                ),
                Container(
                    margin: const EdgeInsets.only(top: 16.0,left: 28.0),
                    alignment: Alignment.topLeft,
                    child: Text('Номер билета',
                    )
                ),
                Container(
                   margin: const EdgeInsets.only(top: 2.0,left: 28.0),
                   alignment: Alignment.topLeft,
                   child:Text(ticketNumber.toString(),
                     style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold
                  ),
                 )
                ),
                SizedBox(
                  height: 40.0,
                ),

                Text('...........................................................'
                    '............................................................',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold
                   ),
                ),
                SizedBox(
                  height: 40.0,
                ),
                 QrImage(
                     data: ticketNumber.toString(),
                     size: 120.0,
                    backgroundColor: Theme.of(context).brightness ==
                        Brightness.dark
                        ? Colors.white : const Color(0x00FFFFFF),
                 ),
                SizedBox(
                  height: 40.0,
                ),
              ]
            )
          ),
               Container(
                   margin: const EdgeInsets.only(bottom: 110.0),
                   child: Text('Покажите этот билет на входе в зал',
                   )
               )
           ]
        )
       )
    );
  }
}