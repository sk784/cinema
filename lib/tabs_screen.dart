import 'package:cinema/screens/films/main_screen.dart';
import 'package:cinema/screens/profile/main_profile.dart';
import 'package:cinema/screens/tickets/my_tickets.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class TabsScreen extends StatefulWidget {

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen>{

  int _currentIndex = 0;

  final _mainScreen = GlobalKey<NavigatorState>();
  final _myTicketsScreen = GlobalKey<NavigatorState>();
  final _mainProfileScreen = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
          index: _currentIndex,
          children: <Widget>[
            WillPopScope(
              onWillPop: () async {
                if ( _mainScreen.currentState.canPop()) {
                  _mainScreen.currentState.pop();
                  return false;
                }
                else if (_myTicketsScreen.currentState.canPop()){
                  _myTicketsScreen.currentState.pop();
                  return false;
                }
                else if (_mainProfileScreen.currentState.canPop()){
                  _mainProfileScreen.currentState.pop();
                  return false;
                }
                return _onWillPop();
              },
              child: Navigator(
                key:_mainScreen,
                onGenerateRoute: (route) => MaterialPageRoute(
                  settings: route,
                  builder: (context) => MainScreen(),
                ),
              ),
            ),
            Navigator(
              key: _myTicketsScreen,
              onGenerateRoute: (route) => MaterialPageRoute(
                settings: route,
                builder: (context) => MyTickets(),
              ),
            ),
             Navigator(
              key:_mainProfileScreen,
              onGenerateRoute: (route) => MaterialPageRoute(
                settings: route,
                builder: (context) => MainProfile(),
              ),
            ),
          ]
      ),
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Theme
            .of(context)
            .brightness == Brightness.light
            ?
        Theme
            .of(context)
            .bottomAppBarTheme
            .color
            :
        Colors.white,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 12.0,
        unselectedFontSize: 12.0,
        currentIndex: _currentIndex,
        onTap: (val) => _onTap(val, context),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        items: [
          BottomNavigationBarItem(
            icon: Icon(LineIcons.film),
            title: Text('Главная'),
          ),
          BottomNavigationBarItem(
            icon: Icon(LineIcons.ticket),
            title: Text('Билеты'),
          ),
          BottomNavigationBarItem(
            icon: Icon(LineIcons.user),
            title: Text('Профиль'),
          ),
        ],
      ),
    );
  }

  void _onTap(int val, BuildContext context) {
    if (_currentIndex == val) {
      switch (val) {
        case 0:
          _mainScreen.currentState.popUntil((route) => route.isFirst);
          break;
        case 1:
          _myTicketsScreen.currentState.popUntil((route) => route.isFirst);
          break;
        case 2:
          _mainProfileScreen.currentState.popUntil((route) => route.isFirst);
          break;
        default:
      }
    } else {
      _mainScreen.currentState.popUntil((route) => route.isFirst);
      _myTicketsScreen.currentState.popUntil((route) => route.isFirst);
      _mainProfileScreen.currentState.popUntil((route) => route.isFirst);
        setState(() {
          _currentIndex = val;
        });
    }
  }

  Future<bool> _onWillPop() async {
    return (
        await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Вы уверены, что хотите выйти из приложения?',
            style: TextStyle(
            fontSize: 16.0,
         ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Нет'),
          ),
          FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text('Да'),
          ),
        ],
      ),
    )) ?? false;
  }
}
