String formatRuntime(int runtime) {
  int hours = runtime ~/ 60;
  int minutes = runtime % 60;

  return '$hours\ч $minutes\м';
}

String formatWeekDay(int weekDay) {
  switch (weekDay) {
    case 1:
      return('Пн');
      break;
    case 2:
      return('Вт');
      break;
    case 3:
      return('Ср');
      break;
    case 4:
      return('Чт');
      break;
    case 5:
      return('Пт');
      break;
    case 6:
      return('Сб');
      break;
    case 7:
      return('Вс');
      break;
    default:
      return('');
      break;
  }
}