import 'package:cinema/model/response/films/prices.dart';

class Film {
  final String id;
  final String title;
  final String country;
  final List<String> media;
  final String shortDesc;
  final String longDesc;
  final int duration;
  final String age;
  final String genre;
  final List<Prices> prices;

  Film(this.id,this.title,this.country,this.media,this.shortDesc,
    this.longDesc,this.duration,this.age,this.genre,this.prices);

}