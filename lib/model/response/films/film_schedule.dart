import 'package:cinema/model/response/films/prices.dart';

class FilmSchedule extends Comparable {
  final int day;
  final List<Prices> prices;
  final bool isActual;

  FilmSchedule(this.day,this.prices,this.isActual);

  @override
  int compareTo(other) {
    int dayComp = this.day.compareTo(other.day);
    return dayComp;
  }
}