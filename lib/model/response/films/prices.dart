class Prices extends Comparable{

  final int time;
  final double price;

  Prices({this.time,this.price});

  Prices.fromJson(Map<String, dynamic> json)
      : time = json['time'],
        price = json['price'];

  @override
  int compareTo(other) {
    int timeComp = this.time.compareTo(other.time);
    return timeComp;
  }
}