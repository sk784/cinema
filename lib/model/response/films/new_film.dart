class NewFilm {
  final String id;
  final String title;
  final String country;
  final List<String> media;
  final String shortDesc;
  final String longDesc;
  final int duration;
  final String age;
  final String genre;
  final int minPrice;
  final int maxPrice;

  NewFilm(this.id,this.title,this.country,this.media,this.shortDesc,
      this.longDesc,this.duration,this.age,this.genre,this.minPrice,this.maxPrice);
}