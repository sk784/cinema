import 'genres.dart';

class StaticLists {
  List<Genres> genres = <Genres>[];

  StaticLists.fromJSON(Map<String, dynamic> json)
      : genres = (json["genres"] as List<dynamic>)
      .map((item) => Genres.fromJson(item)).toList();
}