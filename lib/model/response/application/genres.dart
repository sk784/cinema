class Genres {

  final int id;
  final String value;

  Genres({this.id,this.value});

  Genres.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        value = json['value'];
  }
