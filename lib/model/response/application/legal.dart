class Legal {

  final String terms;
  final String privacy;

  Legal({this. terms,this.privacy});

  factory Legal.fromJson(Map<String, dynamic> json) {
    return Legal(
      terms: json['terms'],
      privacy: json['privacy'],
    );
  }
}