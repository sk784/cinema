class Code {

  final String token;

  Code({this.token});

  factory Code.fromJson(Map<String, dynamic> json) {
    return Code(
        token: json['token']
    );
  }
}