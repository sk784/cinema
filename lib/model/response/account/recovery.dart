class Recovery {

  final String token;
  final String password;

  Recovery({this.token,this.password});

  factory Recovery.fromJson(Map<String, dynamic> json) {
    return Recovery(
      token: json['token'],
      password: json['password'],
    );
  }
}