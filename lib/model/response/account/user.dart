class User {

  final String token;
  final String name;
  final String phone;
  final String lat;
  final String lon;
  final int cinema;
  final int aggregator;

  User({this.token,this.name,this.phone,this.lat,this.lon,this.cinema,this.aggregator});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      token: json['token'],
      name: json['name'],
        phone: json['phone'],
      lat: json['lat'],
      lon: json['lon'],
      cinema: json['notifications']['cinema'],
      aggregator: json['notifications']['aggregator']
    );
  }

}