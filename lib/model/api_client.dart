import 'dart:convert';

import 'package:cinema/model/response/application/genres.dart';
import 'package:cinema/model/response/application/static_lists.dart';
import 'package:cinema/model/response/films/film.dart';
import 'package:cinema/model/response/films/film_schedule.dart';
import 'package:cinema/model/response/films/new_film.dart';
import 'package:cinema/model/response/films/prices.dart';
import 'package:cinema/utils/const.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MovieApi {

  final String baseUrl = 'https://kino-api.senseisoft.com';

  Future<String> _readAppId() async {
    return await rootBundle.loadString('assets/appId.txt');
  }

  MovieApi._privateConstructor();
  static final MovieApi instance = MovieApi._privateConstructor();

  Map <String, String> headers = {'Content-Type': 'application/json'};


  /////////////////SHARED PREFERENCES//////////////////

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<String> getMobileToken() async {
    final SharedPreferences prefs = await _prefs;
    return prefs.getString(Const.TOKEN)
        ??
        '';
  }

  Future<bool> setMobileToken(String token) async {
    final SharedPreferences prefs = await _prefs;
    return prefs.setString(Const.TOKEN, token);
  }

  Future<Map<String, String>> getHeadersWithAuthorization() async {
    final String mobileToken = await getMobileToken();
    return {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $mobileToken'
    };
  }

  /////////////////APPLICATION//////////////////


  Future<http.Response> legalTexts() async {
     String appId = await _readAppId();
    final response = await http.post(baseUrl + "/app/legal",
        body: jsonEncode({
          "appId": appId,
        }), headers: headers);
    return response;
  }

  Future<StaticLists> getStaticLists() async {
    String appId = await _readAppId();
    var data = await http.post(baseUrl + "/app/static",
        body: jsonEncode({
          "appId": appId,
          "lang": "ru"
        }), headers: headers);
    print(data.body);
    print(data.statusCode);
    StaticLists list = StaticLists.fromJSON(json.decode(data.body));
    return list;
  }

  /////////////////ACCOUNT//////////////////

  Future<http.Response> userRegistration(String phone, String password) async {
    String appId = await _readAppId();
    final response = await http.post(baseUrl + "/account/register",
        body: jsonEncode({
          "appId": appId,
          "phone": phone,
          "password": password,
        }), headers: headers);
    return response;
  }

  Future<http.Response> userRequestCode(String phone) async {
    String appId = await _readAppId();
    final response = await http.post(baseUrl + "/account/recovery/request",
        body: jsonEncode({
          "appId": appId,
          "phone": phone,
        }), headers: headers);
    return response;
  }

  Future<http.Response> userUpdate(String body) async {
    final headersWithAuth = await getHeadersWithAuthorization();
    print(headersWithAuth.toString());
    final response = await http.post(baseUrl + "/account/update",
        body: body, headers: headersWithAuth);
    return response;
  }

  Future<http.Response> userLogin(String phone, String password,
      String deviceId,
      String fcmToken) async {
    String appId = await _readAppId();
    print(appId);
    final response = await http.post(baseUrl + "/account/login",
        body: jsonEncode({
          "appId": appId,
          "phone": phone,
          "password": password,
          "deviceId": deviceId,
          "fcmToken": fcmToken,
        }), headers: headers);
    return response;
  }

  Future<http.Response> userLoginByToken() async {
    final headersWithAuth = await getHeadersWithAuthorization();
    final response = await http.post(baseUrl + "/account/login",
        headers: headersWithAuth);
    return response;
  }

  Future<http.Response> userVerifyCode(String phone, String code) async {
    String appId = await _readAppId();
    final response = await http.post(baseUrl + "/account/recovery/verify",
        body: jsonEncode({
          "appId": appId,
          "phone": phone,
          "code": code,
        }), headers: headers);
    return response;
  }

  Future<http.Response> userConfirmCode(String phone, String code) async {
    String appId = await _readAppId();
    final response = await http.post(baseUrl + "/account/confirm",
        body: jsonEncode({
          "appId": appId,
          "phone": phone,
          "code": code,
        }), headers: headers);
    print(response.body);
    return response;
  }

  Future<http.Response> userConfirmCodeFromUpdate(String phone,
      String code) async {
    final headersWithAuth = await getHeadersWithAuthorization();
    final response = await http.post(baseUrl + "/account/update/verify",
        body: jsonEncode({
          "phone": phone,
          "code": code,
        }), headers: headersWithAuth);
    print(response.body);
    return response;
  }

/////////////////FILMS//////////////////

  Future<List<Film>> getMoviesListByDay(int date) async {
    final headersWithAuth = await getHeadersWithAuthorization();
       final staticLists = await getStaticLists();
       final List<Genres> genres = staticLists.genres;
    var data = await http.post(baseUrl + "/movies/list",
        body: jsonEncode({
          "day": date,
        }), headers: headersWithAuth);
    print(data.body);
    var jsonData = json.decode(data.body) as List;
    return jsonData.map((films) {
           Genres element = genres.firstWhere((element) => element.id == films['genre']);
      return Film(
          films['id'],
          films['title'],
          films['country'],
          (films['media'] as List<dynamic>)
              .map<String>((value) => value.toString()).toList(),
          films['shortDesc'],
          films['longDesc'],
          films['duration'],
          films['age'],
          element.value,
          ((films['prices'] as List)
              .map((json) => Prices.fromJson(json))
              .toList()..sort())
      );
    }).toList();
  }


  Future<List<NewFilm>> getHighlightsMovies() async {
    final headersWithAuth = await getHeadersWithAuthorization();

    final staticLists = await getStaticLists();
    final List<Genres> genres = staticLists.genres;

    var data = await http.post(baseUrl + "/movies/highlights",
        headers: headersWithAuth);
    print(data.body);
    var jsonData = json.decode(data.body) as List;

    return jsonData.map((films) {
      Genres element = genres.firstWhere((element) => element.id == films['genre']);
      return NewFilm(
          films['id'],
          films['title'],
          films['country'],
          (films['media'] as List<dynamic>)
              .map<String>((value) => value.toString()).toList(),
          films['shortDesc'],
          films['longDesc'],
          films['duration'],
          films['age'],
          element.value,
          films['minPrice'],
          films['maxPrice']
      );
    }).toList();
  }

  Future<List<FilmSchedule>> getMoviesSchedule(String id) async {
    final headersWithAuth = await getHeadersWithAuthorization();
    var data = await http.post(baseUrl + "/movies/schedule",
        body: jsonEncode({
          "id": id,
        }), headers: headersWithAuth);
    print(data.body);
    var jsonData = json.decode(data.body) as List;

    return jsonData.map((films) {
      return FilmSchedule(
          films['day'],
          ((films['prices'] as List)
              .map((json) => Prices.fromJson(json))
              .toList()..sort()),
          DateFormat("yyyy/MM/dd").parse(films['day'].toString().
          replaceRange(0, 0,"20").replaceRange(4, 4,"/").replaceRange(7, 7,"/"))
              .isAfter(DateTime.now().subtract(Duration(days: 1)))
      );
    }).where((i) => i.isActual).toList()..sort();
  }
}



